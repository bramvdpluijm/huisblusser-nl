@extends('layouts.app')

@section('content')
  @include('partials.content-page')
  @include('partials.product-slider')
  @include('partials.usps')
  @include('partials.product-types')
@endsection
