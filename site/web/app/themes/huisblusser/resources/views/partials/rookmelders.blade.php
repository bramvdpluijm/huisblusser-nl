<section class="focus-comp">
  <div class="container">
    <div class="focus-comp__left">
      <h2>@php the_field('rookmelder_blok_titel') @endphp</h2>
      <p>@php the_field('rookmelder_blok_tekst') @endphp</p>
      <a href="<?php echo get_term_link( 21 ,'product_cat') ?>" class="custom-button custom-button__red">Bekijk assortiment</a>
    </div>
    <div class="focus-comp__right">
      <img src="@asset('images/rookmelder.png')" />
    </div>
  </div>
</section>
