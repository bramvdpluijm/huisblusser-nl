<section class="product-types">
  {{-- getProductCategories function is situated in front-page.php controller --}}
  <div class="container">
    <h2>@php the_field('product_categorieen_titel') @endphp</h2>
    @foreach ($get_product_categories as $cat)
        @if ($cat->category_parent == 0 && $cat->name !== 'Uncategorized' && $cat->name !== 'Omgeving')
            @php
              $woo_cat_img_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
              $woo_cat_img = wp_get_attachment_url( $woo_cat_img_id );
            @endphp
            <a href="@php echo get_term_link( $cat->term_id ,'product_cat') @endphp" class="product-types__card" style="background-image: url('@php echo $woo_cat_img @endphp')">
              <span>@php echo $cat->name @endphp</span>
              <img src="@asset('images/arrow.svg')" />
            </a>
        @endif
    @endforeach
</section>
