@php
  global $post;
  $shop_page_id =  get_option( 'woocommerce_shop_page_id' );
@endphp
<div class="page-header" style="background-image: url('@php the_field('header_foto', $shop_page_id); @endphp')">
  <div class="container">
    @if (!is_shop() && !is_product_category())
      <h1>{!! App::title() !!}</h1>
    @else
      <h1>{{ woocommerce_page_title() }}</h1>
    @endif
    <span>@php the_field('header_subtitel', $shop_page_id); @endphp</span>
  </div>
</div>
