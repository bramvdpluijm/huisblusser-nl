<footer class="footer">
  <div class="container">
    <div class="footer__list">
      <span class="footer__list-title">Huisblusser.nl</span>
      <ul>
        @if( have_rows('footer_lijst_algemeen', 'option') )
          @while ( have_rows('footer_lijst_algemeen', 'option') ) @php the_row(); @endphp
            <li>@php the_sub_field('lijst_item'); @endphp</li>
          @endwhile
        @endif
      </ul>
    </div>
    <div class="footer__list">
      <span class="footer__list-title">Menu</span>
      @if (has_nav_menu('footer_navigation'))
        {!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']) !!}
      @endif
    </div>
    {{-- <div class="footer__list">
      <span class="footer__list-title">Inschrijven Nieuwsbrief</span>
      <form class="footer__form-holder">
        <label for="email">Je e-mailadres:</label>
        <input class="emailfield" type="text" name="email" />
        <label><input type="checkbox" /> Ik ga akkoord met de algemene voorwaarden en privacy wetgeving.</label>
        <button class="custom-button custom-button__white">Aanmelden</button>
      </form>
    </div> --}}
  </div>
</footer>
<div class="bottom-bar">
  <div class="container">
    Copyright © <?php echo date("Y"); ?> - Huisblusser.nl
  </div>
</div>
