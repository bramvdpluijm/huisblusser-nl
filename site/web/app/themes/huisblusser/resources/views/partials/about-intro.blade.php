<div class="about__intro-holder">
  <div class="container">
    <div class="about__intro-element">
      <h2>@php the_field('intro_titel') @endphp</h2>
      <p>@php the_field('intro_tekst') @endphp</p>
    </div>
  </div>
</div>

<div class="about__information-tabs">
  <div class="container">
    <ul data-tabs class="tabs">
      @php $tabitem = 1; @endphp
      @if( have_rows('tabbladen') )
        @while ( have_rows('tabbladen') ) @php the_row(); @endphp
          @if ($tabitem == 1)
            <li class="active">
              <a data-tab href="#@php the_sub_field('tabblad_naam') @endphp">
                @php the_sub_field('tabblad_naam') @endphp
              </a>
            </li>
          @else
            <li>
              <a data-tab href="#@php the_sub_field('tabblad_naam') @endphp">
                @php the_sub_field('tabblad_naam') @endphp
              </a>
            </li>
          @endif
          @php ++$tabitem; @endphp
        @endwhile
      @endif
    </ul>
  </div>

  <div data-tabs-content>
    @php $content = 1; @endphp
    @if( have_rows('tabbladen') )
        @while ( have_rows('tabbladen') ) @php the_row(); @endphp
          @if ($content == 1)
            <div data-tabs-pane class="tabs-pane active" id="@php the_sub_field('tabblad_naam') @endphp">
              <div class="container">
                @php the_sub_field('tabblad_content') @endphp
              </div>
            </div>
          @else
            <div data-tabs-pane class="tabs-pane" id="@php the_sub_field('tabblad_naam') @endphp">
              <div class="container">
                @php the_sub_field('tabblad_content') @endphp
              </div>
            </div>
          @endif
          @php ++$content; @endphp
        @endwhile
    @endif
  </div>
</div>

<div class="about__usp-holder">
  <div class="container">
    <div class="about__usp-element">
      <h2>@php the_field('usp_element_titel') @endphp</h2>
      <ul>
        @if( have_rows('usp_element_usp') )
            @while ( have_rows('usp_element_usp') ) @php the_row(); @endphp
              <li>
                @php the_sub_field('usp_tekst') @endphp
              </li>
            @endwhile
        @endif
      </ul>
    </div>
  </div>
</div>
