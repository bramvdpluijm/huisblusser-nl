<header class="banner">
  <div class="container">
    <a class="brand" href="{{ home_url('/') }}"><img src="@asset('images/huisblusser-logo-rood.svg')" /></a>
    <nav class="nav-primary">
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
      <a href="@php echo get_permalink( wc_get_page_id( 'cart' ) ); @endphp" class="cart-button custom-button custom-button__grey custom-button--icon custom-button--small"><img src="@asset('images/icon-cart.svg')" alt="winkelmand" />Bekijk winkelmand</a>
    </nav>
    <button class="toggle-menu"><img src="@asset('images/icon-menu.svg')" />Menu</button>
  </div>
</header>
