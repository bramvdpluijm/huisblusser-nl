<section class="usps">
  <div class="container">
    <h2 class="usps__title"><?php the_field('intro_tekst_titel'); ?></h2>
    <p class="usps__intro"><?php the_field('intro_tekst'); ?></p>
    <div class="usps__item">
      <div class="icon"><img src="@asset('images/icon-check.svg')" /></div>
      <p><?php the_field('usp1'); ?></p>
    </div>
    <div class="usps__item">
      <div class="icon"><img src="@asset('images/icon-clock.svg')" /></div>
      <p><?php the_field('usp2'); ?></p>
    </div>
    <div class="usps__item">
      <div class="icon"><img src="@asset('images/icon-euro.svg')" /></div>
      <p><?php the_field('usp3'); ?></p>
    </div>
  </div>
</section>
