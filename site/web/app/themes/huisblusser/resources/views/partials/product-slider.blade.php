<section class="product-slider">
  <button class="product-slider__toggle">
    <img src="@asset('images/icon-arrow.svg')" class="arrow" />
    <img src="@asset('images/icon-cross.svg')" class="cross" />
  </button>

  @if( have_rows('product_categorieen') )
    @while ( have_rows('product_categorieen') ) @php the_row(); @endphp
      @php $cat = get_sub_field('product_category'); @endphp
      @php $product_category_link = get_term_link( $cat->term_id, 'product_cat' ); @endphp
      <a href="{{$product_category_link}}" class="cat_navigator {{ $cat->term_id }}"><img src="@asset('images/icon-link.svg')" />Bekijk het {{$cat->name}} winkeloverzicht</a>
    @endwhile
  @endif

  <div class="product-slider__controls">
    <ul>
      @if( have_rows('product_categorieen') )
          @while ( have_rows('product_categorieen') ) @php the_row(); @endphp
            <li>
              @php $cat = get_sub_field('product_category'); @endphp
              <button class="filter-slider" data-filter-id="{{ $cat->term_id }}">{{ $cat->name }}</button>
            </li>
          @endwhile
      @endif
    </ul>
  </div>

  <div class="slider-instructions active">
    <div class="container">
      <div class="instructions">
        <h2>@php the_field('instructie_titel') @endphp</h2>
        <p>@php the_field('instructie_tekst') @endphp</p>
      </div>
    </div>
  </div>

  <div class="product-slider__wrapper">
    @php
      // Get all products with their categories
      $args = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
        'taxonomy'   =>  'product_cat'
      );

      $loop = new WP_Query( $args );
      // End get all products with their categories
    @endphp

      @while ( $loop->have_posts() ) @php $loop->the_post(); @endphp
        @php
          global $product;
          $product_id = $product->get_id();
          $product_image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );
          $product_details = $product->get_data();
          $product_short_description = $product_details['short_description'];
          $product_price = $product->get_price();
          $product_link = get_permalink($product_id);

          $terms = get_the_terms ( $product_id, 'product_cat' );
        @endphp

        <div
          class="slide @php
              foreach ( $terms as $term ) {
                $cat_id = $term->term_id;
                echo $cat_id.' ';
              }
            @endphp">
          <div class="slide__innerwrapper">
              <div class="slide__content">
              <div class="mobile-product-title">
                  <span class="price">vanaf €{{$product_price}},- (incl. BTW)</span>
                  <span class="product-title">{{get_the_title()}}</span>
              </div>

              <div class="slide__product-info desktop">
                  <div>
                    <span class="price">vanaf €{{$product_price}},- (incl. BTW)</span>
                    <span class="product-title">{{get_the_title()}}</span>
                    {!! $product_short_description !!}
                    <a href="{{$product_link}}" class="order-button custom-button custom-button__red">Bestel direct</a>
                  </div>
              </div>

              <div class="slide__product-info mobile">
                  <div>
                    <span class="price">vanaf €{{$product_price}},- (incl. BTW)</span>
                    <span class="product-title">{{get_the_title()}}</span>
                    {!! $product_short_description !!}
                  </div>
              </div>

              <div class="slide__image" style="background-image: url({{$product_image[0]}})"></div>
              <a href="{{$product_link}}" class="order-button custom-button custom-button__red mobile-order-button">Bestel direct</a>
              </div>
          </div>
      </div>
      @endwhile
      {{-- End loop over all products and output their title, apart from that output their category id as a classname for filtering --}}
    @php
      wp_reset_query();
    @endphp
  </div>
</section>
