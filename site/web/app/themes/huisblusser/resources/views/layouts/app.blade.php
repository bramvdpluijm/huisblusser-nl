<!doctype html>
<html @php language_attributes() @endphp>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap" role="document">
      <div class="content">
        <main class="main">
          {{-- Put everything inside a container if not on the homepage or shop page or product category page --}}
          @if (!is_front_page() && !is_shop() && !is_product_category() && !is_page(25) && !is_page(23))
            <div class="container">
              @yield('content')
            </div>
          @else
            @yield('content')
          @endif
        </main>
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
