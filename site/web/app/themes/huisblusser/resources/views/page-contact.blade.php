@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    <div class="container">
      <div class="contact-container">
        <div class="left">
          <h2>@php the_field('contact_titel') @endphp</h2>
          <p class="contact-intro">@php the_field('contact_tekst') @endphp</p>
          @include('partials.content-page')
        </div>
        <div class="right">
          <div class="contact-card">
            <div class="contact-image" style="background-image: url('@php the_field('contact_image') @endphp')"></div>
            <div class="contact-info">
              <span>CONTACTGEGEVENS</span>
              <ul>
                @if( have_rows('contact_lijst') )
                    @while ( have_rows('contact_lijst') ) @php the_row(); @endphp
                      <li>
                        @php the_sub_field('contact_regel') @endphp
                      </li>
                    @endwhile
                @endif
                <li><a href="mailto:info@huisblusser.nl">info@huisblusser.nl</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endwhile
@endsection
