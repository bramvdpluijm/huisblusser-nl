@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  <div class="container product-container">
    @if(woocommerce_product_loop())
      <div class="product-controls">
        @php
          do_action('woocommerce_before_shop_loop');
        @endphp
      </div>

      @php woocommerce_product_loop_start(); @endphp

      @if(wc_get_loop_prop('total'))
        @while(have_posts())
          @php
            the_post();
            do_action('woocommerce_shop_loop');
            wc_get_template_part('content', 'product');
          @endphp
        @endwhile
      @endif

      @php
        woocommerce_product_loop_end();
        do_action('woocommerce_after_shop_loop');
      @endphp
    @else
      @php
        do_action('woocommerce_no_products_found');
      @endphp
    @endif
  </div>

  @php
    do_action('woocommerce_after_main_content');
    do_action('get_sidebar', 'shop');
    do_action('get_footer', 'shop');
  @endphp
@endsection
