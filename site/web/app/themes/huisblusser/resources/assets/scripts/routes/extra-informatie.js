/* eslint-disable */
import tabby from 'Tabby';

export default {
  init() {
    // JavaScript to be fired on the extra-informatie page
    tabby.init();
  },
  finalize() {
  },
};
