export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    const mobileToggle = document.querySelector('.toggle-menu');
    const menuElement = document.querySelector('.nav-primary');
    mobileToggle.addEventListener('click', () => {
      menuElement.classList.toggle('open');
    })
  },
};
