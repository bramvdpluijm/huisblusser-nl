export default {
  init() {
    const arrow = require('../../images/arrow.svg');
    // JavaScript to be fired on the home page
    $('.product-slider__wrapper').slick({
      dots: true,
      arrows: true,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true,
      centerPadding: '0px',
      adaptiveHeight: true,
      prevArrow: `<div class="product-slider__control left"><img src="${arrow}" /><span class="arrow-text vorige">Vorige</span></div>`,
      nextArrow: `<div class="product-slider__control right"><img src="${arrow}" /><span class="arrow-text volgende">Volgende</span></div>`,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            arrows: false,
          },
        },
      ],
    });

    // Filter product slider on product category id

    $('.filter-slider').on('click', function() {
      // Show tthe slider after user clicked on filter item, hide the slider instructions
      $('.product-slider__wrapper').addClass('active');
      $('.slider-instructions').removeClass('active');
      // END -> Show tthe slider after user clicked on filter item, hide the slider instructions

      $('.product-slider__wrapper').slick('slickUnfilter');
      const filter_id = $(this).data('filter-id');
      const filterSliders = document.querySelectorAll('.filter-slider');
      const cat_navigators = document.querySelectorAll('.cat_navigator');

      for(let i = 0; i < filterSliders.length; i++) {
        filterSliders[i].classList.remove('active');
      }

      for(let c = 0; c < cat_navigators.length; c++) {
        cat_navigators[c].classList.remove('active');
        if (cat_navigators[c].classList.contains($(this).data('filter-id'))) {
          cat_navigators[c].classList.add('active');
        }
      }

      $(this).addClass('active');
      $('.product-slider__wrapper').slick('slickFilter', $(`.${filter_id}`));
    });

    // End filter product slider on product category id
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
    const filterToggle = document.querySelector('.product-slider__toggle');
    const filterControls = document.querySelector('.product-slider__controls');

    filterToggle.addEventListener('click', () => {
      filterControls.classList.toggle('open')
      filterToggle.classList.toggle('open')
    });
  },
};
